create or replace NONEDITIONABLE PACKAGE BODY SPREMI AS
e_iznimka exception;

--save_klijenti
-----------------------------------------------------------------------------------------
  procedure p_save_klijenti(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) AS
      l_obj JSON_OBJECT_T;
      l_klijenti klijenti%rowtype;
      l_count number;
      l_id number;
      l_string varchar2(1000);
      l_search varchar2(100);
      l_page number; 
      l_perpage number;
      l_action varchar2(10);
  begin
  
     l_obj := JSON_OBJECT_T(in_json);  
     l_string := in_json.TO_STRING;
  
     SELECT
        JSON_VALUE(l_string, '$.ID' ),
        JSON_VALUE(l_string, '$.IME'),
        JSON_VALUE(l_string, '$.PREZIME' ),
        JSON_VALUE(l_string, '$.EMAIL' ),
        JSON_VALUE(l_string, '$.OIB' ),
        JSON_VALUE(l_string, '$.OVLASTI' ),
        JSON_VALUE(l_string, '$.SPOL' ),
        JSON_VALUE(l_string, '$.ZAPORKA' ),
        JSON_VALUE(l_string, '$.ACTION' )
    INTO
        l_klijenti.id,
        l_klijenti.IME,
        l_klijenti.PREZIME,
        l_klijenti.EMAIL,
        l_klijenti.OIB,
        l_klijenti.OVLASTI,
        l_klijenti.SPOL,
        l_klijenti.PASSWORD,
        l_action
    FROM 
       dual; 
       
    --FE kontrole
    if (nvl(l_action, ' ') = ' ') then
        if (filter.f_check_klijenti(l_obj, out_json)) then
           raise e_iznimka; 
        end if;  
    end if;
           
    if (l_klijenti.id is null) then
        begin
           insert into klijenti (IME, PREZIME, EMAIL, OIB, OVLASTI, SPOL, PASSWORD) values
             (l_klijenti.IME, l_klijenti.PREZIME,
              l_klijenti.EMAIL, l_klijenti.OIB, l_klijenti.OVLASTI,
              l_klijenti.SPOL, l_klijenti.PASSWORD);
           commit;
           
           l_obj.put('h_message', 'Uspješno ste unijeli klijenta'); 
           l_obj.put('h_errcode', 0);
           out_json := l_obj;
           
        exception
           when others then 
               --COMMON.p_errlog('p_users',dbms_utility.format_error_backtrace,SQLCODE,SQLERRM, l_string);
               rollback;
               raise;
        end;
    else
       if (nvl(l_action, ' ') = 'delete') then
           begin
               delete klijenti where id = l_klijenti.id;
               commit;    
               
               l_obj.put('h_message', 'Uspješno ste obrisali klijenta'); 
               l_obj.put('h_errcode', 0);
               out_json := l_obj;
            exception
               when others then 
                   --COMMON.p_errlog('p_users',dbms_utility.format_error_backtrace,SQLCODE,SQLERRM, l_string);
                   rollback;
                   raise;
            end;
       
       else
       
           begin
               update klijenti 
                  set IME = l_klijenti.IME,
                      PREZIME = l_klijenti.PREZIME,
                      EMAIL = l_klijenti.EMAIL,
                      OIB = l_klijenti.OIB,
                      OVLASTI = l_klijenti.OVLASTI, 
                      SPOL = l_klijenti.SPOL
               where
                  id = l_klijenti.id;
               commit;    
               
               l_obj.put('h_message', 'Uspješno ste promijenili klijenta'); 
               l_obj.put('h_errcode', 0);
               out_json := l_obj;
            exception
               when others then 
                   --COMMON.p_errlog('p_users',dbms_utility.format_error_backtrace,SQLCODE,SQLERRM, l_string);
                   rollback;
                   raise;
            end;
       end if;     
    end if;
    
    
  exception
     when e_iznimka then
        out_json := l_obj; 
     when others then
        --COMMON.p_errlog('p_save_klijenti',dbms_utility.format_error_backtrace,SQLCODE,SQLERRM, l_string);
        l_obj.put('h_message', 'Dogodila se greška u obradi podataka!'); 
        l_obj.put('h_errcode', 101);
        out_json := l_obj;
  END p_save_klijenti;




--save_zanr
-----------------------------------------------------------------------------------------
  procedure p_save_zanr(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) AS
      l_obj JSON_OBJECT_T;
      l_zanr zanr%rowtype;
      l_count number;
      l_id number;
      l_string varchar2(1000);
      l_search varchar2(100);
      l_page number; 
      l_perpage number;
      l_Action varchar2(10);
      
  BEGIN
     l_obj := JSON_OBJECT_T(in_json);  
     l_string := in_json.TO_STRING;
  
     SELECT
        JSON_VALUE(l_string, '$.ID' ),
        JSON_VALUE(l_string, '$.NAZIV'),
        JSON_VALUE(l_string, '$.ACTION')
    INTO
        l_zanr.ID,
        l_zanr.NAZIV,
        l_Action
    FROM 
       dual; 
       
    --FE kontrole
    if (nvl(l_Action, ' ') = ' ') then
        if (filter.f_check_zanr(l_obj, out_json)) then
           raise e_iznimka; 
        end if;  
    end if;
           
    if (l_zanr.id is null) then
        begin
           insert into zanr (NAZIV) values
             (l_zanr.NAZIV);
           commit;
           
           l_obj.put('h_message', 'Uspješno ste unijeli novi zanr'); 
           l_obj.put('h_errcode', 0);
           out_json := l_obj;
           
        exception
           when others then 
               --COMMON.p_errlog('p_users',dbms_utility.format_error_backtrace,SQLCODE,SQLERRM, l_string);
               rollback;
               raise;
        end;
    else
       if (nvl(l_Action, ' ') = 'delete') then
           begin
               delete zanr where id = l_zanr.id;
               commit;    
               
               l_obj.put('h_message', 'Uspješno ste obrisali zanr'); 
               l_obj.put('h_errcode', 0);
               out_json := l_obj;
            exception
               when others then 
                   --COMMON.p_errlog('p_users',dbms_utility.format_error_backtrace,SQLCODE,SQLERRM, l_string);
                   rollback;
                   raise;
            end;
       
       else
       
           begin
               update zanr 
                  set NAZIV = l_zanr.NAZIV
               where
                  id = l_zanr.ID;
               commit;    
               
               l_obj.put('h_message', 'Uspješno ste promijenili zanr'); 
               l_obj.put('h_errcode', 0);
               out_json := l_obj;
            exception
               when others then 
                   --COMMON.p_errlog('p_users',dbms_utility.format_error_backtrace,SQLCODE,SQLERRM, l_string);
                   rollback;
                   raise;
            end;
       end if;     
    end if;
    
    
  exception
     when e_iznimka then
        out_json := l_obj; 
     when others then
        --COMMON.p_errlog('p_save_klijenti',dbms_utility.format_error_backtrace,SQLCODE,SQLERRM, l_string);
        l_obj.put('h_message', 'Dogodila se greška u obradi podataka!'); 
        l_obj.put('h_errcode', 101);
        out_json := l_obj;
  END p_save_zanr;


--save_glumac
-----------------------------------------------------------------------------------------
 procedure p_save_glumac(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) AS
      l_obj JSON_OBJECT_T;
      l_glumac glumac%rowtype;
      l_count number;
      l_id number;
      l_string varchar2(1000);
      l_search varchar2(100);
      l_page number; 
      l_perpage number;
      l_action varchar2(10);
  begin
  
     l_obj := JSON_OBJECT_T(in_json);  
     l_string := l_obj.TO_STRING;
  
     SELECT
        JSON_VALUE(l_string, '$.ID' ),
        JSON_VALUE(l_string, '$.IME'),
        JSON_VALUE(l_string, '$.PREZIME' ),
        JSON_VALUE(l_string, '$.DRZAVLJANSTVO' ),
        JSON_VALUE(l_string, '$.ACTION' )
    INTO
        l_glumac.id,
        l_glumac.IME,
        l_glumac.PREZIME,
        l_glumac.DRZAVLJANSTVO,
        l_action
    FROM 
       dual; 
       
    --FE kontrole
    if (nvl(l_action, ' ') = ' ') then
        if (filter.f_check_glumac(l_obj, out_json)) then
           raise e_iznimka; 
        end if;  
    end if;
           
    if (l_glumac.id is null) then
        begin
           insert into glumac (IME, PREZIME, DRZAVLJANSTVO) values
             (l_glumac.IME, l_glumac.PREZIME, l_glumac.DRZAVLJANSTVO);
           commit;
           
           l_obj.put('h_message', 'Uspješno ste unijeli glumca'); 
           l_obj.put('h_errcode', 0);
           out_json := l_obj;
           
        exception
           when others then 
               --COMMON.p_errlog('p_users',dbms_utility.format_error_backtrace,SQLCODE,SQLERRM, l_string);
               rollback;
               raise;
        end;
    else
       if (nvl(l_action, ' ') = 'delete') then
           begin
               delete glumac where id = l_glumac.id;
               commit;    
               
               l_obj.put('h_message', 'Uspješno ste obrisali glumca'); 
               l_obj.put('h_errcode', 0);
               out_json := l_obj;
            exception
               when others then 
                   --COMMON.p_errlog('p_users',dbms_utility.format_error_backtrace,SQLCODE,SQLERRM, l_string);
                   rollback;
                   raise;
            end;
       
       else
       
           begin
               update glumac 
                  set IME = l_glumac.IME,
                      PREZIME = l_glumac.PREZIME,
                      DRZAVLJANSTVO = l_glumac.DRZAVLJANSTVO
               where
                  id = l_glumac.id;
               commit;    
               
               l_obj.put('h_message', 'Uspješno ste promijenili glumca'); 
               l_obj.put('h_errcode', 0);
               out_json := l_obj;
            exception
               when others then 
                   --COMMON.p_errlog('p_users',dbms_utility.format_error_backtrace,SQLCODE,SQLERRM, l_string);
                   rollback;
                   raise;
            end;
       end if;     
    end if;
    
    
  exception
     when e_iznimka then
        out_json := l_obj; 
     when others then
        --COMMON.p_errlog('p_save_klijenti',dbms_utility.format_error_backtrace,SQLCODE,SQLERRM, l_string);
        l_obj.put('h_message', 'Dogodila se greška u obradi podataka!'); 
        l_obj.put('h_errcode', 101);
        out_json := l_obj;
  END p_save_glumac;


--save_reziser
-----------------------------------------------------------------------------------------
  procedure p_save_reziser(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) AS
      l_obj JSON_OBJECT_T;
      l_reziser reziser%rowtype;
      l_count number;
      l_id number;
      l_string varchar2(1000);
      l_search varchar2(100);
      l_page number; 
      l_perpage number;
      l_action varchar2(10);
  begin
  
     l_obj := JSON_OBJECT_T(in_json);  
     l_string := in_json.TO_STRING;
  
     SELECT
        JSON_VALUE(l_string, '$.ID' ),
        JSON_VALUE(l_string, '$.IME'),
        JSON_VALUE(l_string, '$.PREZIME' ),
        JSON_VALUE(l_string, '$.DRZAVLJANSTVO' ),
        JSON_VALUE(l_string, '$.ACTION' )
    INTO
        l_reziser.ID,
        l_reziser.IME,
        l_reziser.PREZIME,
        l_reziser.DRZAVLJANSTVO,
        l_action
    FROM 
       dual; 
       
    --FE kontrole
    if (nvl(l_action, ' ') = ' ') then
        if (filter.f_check_reziser(l_obj, out_json)) then
           raise e_iznimka; 
        end if;  
    end if;
           
    if (l_reziser.ID is null) then
        begin
           insert into reziser (IME, PREZIME, DRZAVLJANSTVO) values
             (l_reziser.IME, l_reziser.PREZIME,
              l_reziser.DRZAVLJANSTVO);
           commit;
           
           l_obj.put('h_message', 'Uspješno ste unijeli rezisera'); 
           l_obj.put('h_errcode', 0);
           out_json := l_obj;
           
        exception
           when others then 
               --COMMON.p_errlog('p_users',dbms_utility.format_error_backtrace,SQLCODE,SQLERRM, l_string);
               rollback;
               raise;
        end;
    else
       if (nvl(l_action, ' ') = 'delete') then
           begin
               delete reziser where ID = l_reziser.ID;
               commit;    
               
               l_obj.put('h_message', 'Uspješno ste obrisali rezisera'); 
               l_obj.put('h_errcode', 0);
               out_json := l_obj;
            exception
               when others then 
                   --COMMON.p_errlog('p_users',dbms_utility.format_error_backtrace,SQLCODE,SQLERRM, l_string);
                   rollback;
                   raise;
            end;
       
       else
       
           begin
               update reziser 
                  set IME = l_reziser.IME,
                      PREZIME = l_reziser.PREZIME,
                      DRZAVLJANSTVO = l_reziser.DRZAVLJANSTVO
               where
                  ID = l_reziser.ID;
               commit;    
               
               l_obj.put('h_message', 'Uspješno ste promijenili rezisera'); 
               l_obj.put('h_errcode', 0);
               out_json := l_obj;
            exception
               when others then 
                   --COMMON.p_errlog('p_users',dbms_utility.format_error_backtrace,SQLCODE,SQLERRM, l_string);
                   rollback;
                   raise;
            end;
       end if;     
    end if;
    
    
  exception
     when e_iznimka then
        out_json := l_obj; 
     when others then
        --COMMON.p_errlog('p_save_klijenti',dbms_utility.format_error_backtrace,SQLCODE,SQLERRM, l_string);
        l_obj.put('h_message', 'Dogodila se greška u obradi podataka!'); 
        l_obj.put('h_errcode', 101);
        out_json := l_obj;
  END p_save_reziser;


END SPREMI;
