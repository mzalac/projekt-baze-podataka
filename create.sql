--dropanje tablica
drop table Film;
drop table Reziser;
drop table Glumac;
drop table Zanr;

--dropanje sekvenci
drop sequence FILM_ID_SEQ;
drop sequence REZISER_ID_SEQ;
drop sequence GLUMAC_ID_SEQ;
drop sequence ZANR_ID_SEQ;


--stvaranje tablica
CREATE TABLE Film (
	ID NUMBER(8, 0) NOT NULL,
	IDreziser NUMBER(8, 0) NOT NULL,
	IDzanr NUMBER(8, 0) NOT NULL,
	IDglumac NUMBER(8, 0) NOT NULL,
	Naslov VARCHAR2(30) NOT NULL,
	Godina NUMBER(4, 0) NOT NULL,
	Ocjena NUMBER(2, 0) NOT NULL,
	constraint FILM_PK PRIMARY KEY (ID));

CREATE sequence FILM_ID_SEQ;

CREATE trigger BI_FILM_ID
  before insert on Film
  for each row
begin
  select FILM_ID_SEQ.nextval into :NEW.ID from dual;
end;

/
CREATE TABLE Reziser (
	ID NUMBER(8, 0) NOT NULL,
	Ime VARCHAR2(20) NOT NULL,
	Prezime VARCHAR2(20) NOT NULL,
	Drzavljanstvo VARCHAR2(20) NOT NULL,
	constraint REZISER_PK PRIMARY KEY (ID));

CREATE sequence REZISER_ID_SEQ;

CREATE trigger BI_REZISER_ID
  before insert on Reziser
  for each row
begin
  select REZISER_ID_SEQ.nextval into :NEW.ID from dual;
end;

/
CREATE TABLE Glumac (
	ID NUMBER(8, 0) NOT NULL,
	Ime VARCHAR2(20) NOT NULL,
	Prezime VARCHAR2(20) NOT NULL,
	Drzavljanstvo VARCHAR2(20) NOT NULL,
	constraint GLUMAC_PK PRIMARY KEY (ID));

CREATE sequence GLUMAC_ID_SEQ;

CREATE trigger BI_GLUMAC_ID
  before insert on Glumac
  for each row
begin
  select GLUMAC_ID_SEQ.nextval into :NEW.ID from dual;
end;

/
CREATE TABLE Zanr (
	ID NUMBER(8, 0) NOT NULL,
	Naziv VARCHAR2(30) NOT NULL,
	constraint ZANR_PK PRIMARY KEY (ID));

CREATE sequence ZANR_ID_SEQ;

CREATE trigger BI_ZANR_ID
  before insert on Zanr
  for each row
begin
  select ZANR_ID_SEQ.nextval into :NEW.ID from dual;
end;

/
ALTER TABLE Film ADD CONSTRAINT Film_fk0 FOREIGN KEY (IDreziser) REFERENCES Reziser(ID);
ALTER TABLE Film ADD CONSTRAINT Film_fk1 FOREIGN KEY (IDzanr) REFERENCES Zanr(ID);
ALTER TABLE Film ADD CONSTRAINT Film_fk2 FOREIGN KEY (IDglumac) REFERENCES Glumac(ID);
