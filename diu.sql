---- Unosenje  zanra -----
insert into Zanr (ID, Naziv) values (1, 'Akcija');
insert into Zanr (ID, Naziv) values (0, 'Avantura');
insert into Zanr (ID, Naziv) values (3, 'Drama');
insert into Zanr (ID, Naziv) values (4, 'Komedija');
insert into Zanr (ID, Naziv) values (5, 'Horor');
insert into Zanr (ID, Naziv) values (6, 'Kriminalisticki');
insert into Zanr (ID, Naziv) values (7, 'Romantika');
insert into Zanr (ID, Naziv) values (8, 'Povijesni');
insert into Zanr (ID, Naziv) values (9, '..');
insert into Zanr (ID, Naziv) values (10, 'Mizerija');


---- Unosenje rezisera ----
insert into Reziser (ID, Ime, Prezime, Drzavljanstvo) values (1, 'John', 'Watts', 'Hrvatska');
insert into Reziser (ID, Ime, Prezime, Drzavljanstvo) values (2, 'Todd', 'Phillips', 'SAD');
insert into Reziser (ID, Ime, Prezime, Drzavljanstvo) values (3, 'James', 'Cameron', 'Kanada');
insert into Reziser (ID, Ime, Prezime, Drzavljanstvo) values (4, 'Quentin', 'Tarantino', 'SAD');
insert into Reziser (ID, Ime, Prezime, Drzavljanstvo) values (5, 'Frank', 'Darabont', 'Francuska');
insert into Reziser (ID, Ime, Prezime, Drzavljanstvo) values (6, 'Vince', 'Gilligan', 'SAD');
insert into Reziser (ID, Ime, Prezime, Drzavljanstvo) values (7, 'Martin', 'Scorsese', 'SAD');
insert into Reziser (ID, Ime, Prezime, Drzavljanstvo) values (8, 'Frank', 'Darabont', 'Francuska');
insert into Reziser (ID, Ime, Prezime, Drzavljanstvo) values (9, 'Robert', 'Zemeckis', 'SAD');
insert into Reziser (ID, Ime, Prezime, Drzavljanstvo) values (10, 'Bong', 'Joon Ho', 'Juzna Koreja');
insert into Reziser (ID, Ime, Prezime, Drzavljanstvo) values (11, 'Cristopher', 'Nolan', 'UK');


---- Unosenje glumaca ----
insert into Glumac (ID, Ime, Prezime, Drzavljanstvo) values (1, 'Tom', 'Holland', 'UK');
insert into Glumac (ID, Ime, Prezime, Drzavljanstvo) values (2, 'Joaquin', 'Phoenix', 'Portoriko');
insert into Glumac (ID, Ime, Prezime, Drzavljanstvo) values (3, 'Leonardo', 'DiCaprio', 'SAD');
insert into Glumac (ID, Ime, Prezime, Drzavljanstvo) values (4, 'Tim', 'Robbins', 'SAD');
insert into Glumac (ID, Ime, Prezime, Drzavljanstvo) values (5, 'John', 'Travolta', 'SAD');
insert into Glumac (ID, Ime, Prezime, Drzavljanstvo) values (6, 'Aaron', 'Paul', 'SAD');
insert into Glumac (ID, Ime, Prezime, Drzavljanstvo) values (7, 'Tom', 'Banks', 'SAD');
insert into Glumac (ID, Ime, Prezime, Drzavljanstvo) values (8, 'Song', 'Kang-Ho', 'Juzna Koreja');
insert into Glumac (ID, Ime, Prezime, Drzavljanstvo) values (9, 'Guy', 'Pearce', 'UKK');
insert into Glumac (ID, Ime, Prezime, Drzavljanstvo) values (10, 'Christian', 'Sale', 'UK');


---- Unosenje Filmova ----
insert into Film (ID, IDreziser, IDzanr, IDglumac, Naslov, Godina, Ocjena) values (1, 1, 1, 1, 'Spider-Man : Daleko od kuce', 2017, 3);
insert into Film (ID, IDreziser, IDzanr, IDglumac, Naslov, Godina, Ocjena) values (2, 1, 1, 1, 'Spider-Man : blabla', 2019, 7);
insert into Film (ID, IDreziser, IDzanr, IDglumac, Naslov, Godina, Ocjena) values (3, 2, 6, 2, 'Joker', 2019, 8);
insert into Film (ID, IDreziser, IDzanr, IDglumac, Naslov, Godina, Ocjena) values (4, 3, 10, 3, 'Titanik', 1997, 7);
insert into Film (ID, IDreziser, IDzanr, IDglumac, Naslov, Godina, Ocjena) values (5, 5, 3, 4, 'Iskupljenje u Shawshanku', 1994, 9);
insert into Film (ID, IDreziser, IDzanr, IDglumac, Naslov, Godina, Ocjena) values (6, 4, 6, 5, 'Pakleni sund', 1994, 8);
insert into Film (ID, IDreziser, IDzanr, IDglumac, Naslov, Godina, Ocjena) values (7, 7, 10, 3, 'Otok Shutter', 2010, 8);
insert into Film (ID, IDreziser, IDzanr, IDglumac, Naslov, Godina, Ocjena) values (8, 6, 6, 6, 'El Camino', 2000, 7);
insert into Film (ID, IDreziser, IDzanr, IDglumac, Naslov, Godina, Ocjena) values (9, 9, 3, 7, 'Forrest Gump', 1994, 8);
insert into Film (ID, IDreziser, IDzanr, IDglumac, Naslov, Godina, Ocjena) values (10, 10, 3, 8, 'Parazit', 2019, 8);
insert into Film (ID, IDreziser, IDzanr, IDglumac, Naslov, Godina, Ocjena) values (11, 11, 10, 9, 'Memento', 2000, 8);


--update
update Film set Naslov='Spider-Man : Daleko od kuce' where ID=1;
update Film set Ocjena=7 where Naslov='Spider-Man : Povratak kuci'
update Film set IDZanr=6 where ID=3
update Film set Godina=1997 where Naslov='Titanik'
update Film set Godina=2019 where IDreziser=6
update Film set IDglumac=14 where Naslov='Forrest Gump'
update Film set IDreziser=10 where IDzanr=3 and Godina=2019
update Zanr set Naziv='Misterija' where Naziv='Mizerija';
update Zanr set Naziv='Fantazija' where Naziv='..'
update Reziser set Drzavljanstvo='SAD' where Ime='John' and Prezime='Watts'
update Glumac set Prezime='Hanks' where ID=7
update Glumac set Prezime='Bale' where Prezime='Sale'
update Film set Godina=2000 where Godina=-2000
update Film set Ocjena=1 where Godina=2017 or ID=5
update Film set Ocjena=Ocjena+1
update Glumac set Ime='Promjena' where Drzavljanstvo='SAD' or Drzavljanstvo='Njemacka'
update Reziser set Prezime='.' where Prezime='Nolan'
update Film set Godina=1990
update Film set Ocjena = 10 where Naslov like 'S%'

--delete
delete from Glumac where ID=10
delete from Film where Godina=2019
delete from Zanr where ID=1 
delete from Reziser where Drzavljanstvo='UK'
delete from Film where Ocjena > 7
Delete from Film
delete from Reziser where Ime = 'John' 
delete from Glumac 
