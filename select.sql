----1. select
select
    F.Naslov,
    F.Godina,
    R.Ime || ' ' || R.Prezime as Ime_Prezime
from
    Film F,
    Reziser R
where
    R.ID = F.IDreziser

----2. select
select
    F.Naslov,
    F.Godina,
    F.Ocjena,
    G.Ime || ' ' || G.Prezime as Glavna_Uloga
from
    Film F,
    Glumac G
where 
    G.ID = F.IDGlumac
    
    
----3. select
select
    *
from
    Film F,
    Zanr Z
where
    Z.ID = F.IDzanr
    

----4. select
select
    Naslov,
    Ocjena
from
    Film
where
    Ocjena = 7


----5. select
select
    count(1) as Reziseri_iz_SAD
from    
    Reziser
where
    Drzavljanstvo = 'SAD'
    
    
----6. select
select 
    *
from
    Film
where
    Godina = 2019


----7. select
select
    *
from    
    Film
where
    Ocjena < 8



----8. select
select 
    F.Naslov,
    F.Godina,
    G.Ime || ' ' || G.Prezime as Glumac,
    R.Ime || ' ' || R.Prezime as Reziser,
    Z.Naziv as Zanr
from
    Film F,
    Glumac G,
    Reziser R,
    Zanr Z
where
    F.IDglumac = G.ID AND
    F.IDreziser = R.ID AND
    F.IDzanr = Z.ID


----9. select
select
    F.Naslov,
    F.Godina,
    Z.Naziv as Zanr,
    R.Ime || ' ' || R.Prezime as Reziser,
    G.Ime || ' ' || G.Prezime as Glumac
from
    Film F,
    Zanr Z,
    Reziser R,
    Glumac G
where
    F.IDzanr = Z.ID AND
    F.IDreziser = R.ID AND
    F.IDGlumac = G.ID
    
    
    
----10. select
select
    ID, Naslov, Godina,
    case IDReziser
    when 1 then
        'John Watts'
    when 2 then
        'Todd Phillips'
    when 3 then
        'James Cameron'
    when 4 then
        'Quentin Tarantino'
    when 5 then
        'Frank Darabont'
    when 6 then
        'Vince Gilligan'
    when 7 then
        'Martin Scorsese'
    when 8 then
        'Frank Darabont'
    when 9 then
        'Robert Zemeckis'
    when 10 then
        'Bong Joon Ho'
    when 11 then
        'Cristopher Nolan'
    end as Reziser,
    
    case IDGlumac
    when 1 then
        'Tom Holland'
    when 2 then
        'Joaquin Phoenix'
    when 3 then
        'Leonardo diCaprio'
    when 4 then
        'Tim Robbins'
    when 5 then
        'John Travolta'
    when 6 then
        'Aaron Paul'
    when 7 then
        'Tom Hanks'
    when 8 then
        'Song Kang-Ho'
    when 9 then
        'Guy Pearce'
    end as Glumac
from
    Film
