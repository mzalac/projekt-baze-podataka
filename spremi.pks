create or replace NONEDITIONABLE PACKAGE SPREMI AS 

 procedure p_save_klijenti(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T);
 procedure p_save_zanr(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T);
 procedure p_save_glumac(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T);
 procedure p_save_reziser(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T);

END SPREMI;
