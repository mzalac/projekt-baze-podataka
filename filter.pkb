create or replace NONEDITIONABLE PACKAGE BODY FILTER AS
e_iznimka exception;


--check_klijenti
--------------------------------------------
  function f_check_klijenti(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) return boolean AS
      l_obj JSON_OBJECT_T;
      l_klijenti klijenti%rowtype;
      l_count number;
      l_id number;
      l_string varchar2(1000);
      l_search varchar2(100);
      l_page number; 
      l_perpage number;
  BEGIN
     l_obj := JSON_OBJECT_T(in_json);  
     l_string := in_json.TO_STRING;
    SELECT
        JSON_VALUE(l_string, '$.ID' ),
        JSON_VALUE(l_string, '$.IME'),
        JSON_VALUE(l_string, '$.PREZIME' ),
        JSON_VALUE(l_string, '$.EMAIL' ),
        JSON_VALUE(l_string, '$.OIB' ),
        JSON_VALUE(l_string, '$.OVLASTI' ),
        JSON_VALUE(l_string, '$.SPOL' ),
        JSON_VALUE(l_string, '$.ZAPORKA' )
    INTO
        l_klijenti.id,
        l_klijenti.IME,
        l_klijenti.PREZIME,
        l_klijenti.EMAIL,
        l_klijenti.OIB,
        l_klijenti.OVLASTI,
        l_klijenti.SPOL,
        l_klijenti.PASSWORD
    FROM 
       dual; 

    if (nvl(l_klijenti.IME, ' ') = ' ') then   
       l_obj.put('h_message', 'Molimo unesite ime klijenta'); 
       l_obj.put('h_errcode', 101);
       raise e_iznimka;
    end if;

    if (nvl(l_klijenti.PREZIME, ' ') = ' ') then   
       l_obj.put('h_message', 'Molimo unesite prezime klijenta'); 
       l_obj.put('h_errcode', 102);
       raise e_iznimka;
    end if;

    if (nvl(l_klijenti.EMAIL, ' ') = ' ') then   
       l_obj.put('h_message', 'Molimo unesite email klijenta'); 
       l_obj.put('h_errcode', 103);
       raise e_iznimka;
    end if;

    if (nvl(l_klijenti.OIB, 0) = 0) then   
       l_obj.put('h_message', 'Molimo unesite OIB klijenta'); 
       l_obj.put('h_errcode', 104);
       raise e_iznimka;
    end if;

    if (nvl(l_klijenti.OVLASTI, 99) = 99) then   
       l_obj.put('h_message', 'Molimo odaberite ovlasti klijenta'); 
       l_obj.put('h_errcode', 105);
       raise e_iznimka;
    end if;

    if (nvl(l_klijenti.SPOL, 99) = 99) then   
       l_obj.put('h_message', 'Molimo unesite spol klijenta'); 
       l_obj.put('h_errcode', 106);
       raise e_iznimka;
    else
       if (l_klijenti.SPOL not in (0,1)) then
          l_obj.put('h_message', 'Spol može biti ili 0 ili 1'); 
          l_obj.put('h_errcode', 107);
          raise e_iznimka;
       end if;
    end if;



    if (nvl(l_klijenti.id,0) = 0 and nvl(l_klijenti.PASSWORD, ' ') = ' ') then   
       l_obj.put('h_message', 'Molimo unesite zaporku klijenta'); 
       l_obj.put('h_errcode', 108);
       raise e_iznimka;
    end if;

    out_json := l_obj;
    return false;

  EXCEPTION
     WHEN E_IZNIMKA THEN
            return true;
     WHEN OTHERS THEN
        --COMMON.p_errlog('p_check_klijenti',dbms_utility.format_error_backtrace,SQLCODE,SQLERRM, l_string);
        l_obj.put('h_message', 'Dogodila se greška u obradi podataka!'); 
        l_obj.put('h_errcode', 109);
        out_json := l_obj;
        return true;
  END f_check_klijenti;



--check_zanr
--------------------------------------------
  function f_check_zanr(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) return boolean AS
      l_obj JSON_OBJECT_T;
      l_zanr zanr%rowtype;
      l_count number;
      l_id number;
      l_string varchar2(1000);
      l_search varchar2(100);
      l_page number; 
      l_perpage number;
  BEGIN
     l_obj := JSON_OBJECT_T(in_json);  
     l_string := in_json.TO_STRING;

     SELECT
        JSON_VALUE(l_string, '$.ID' ),
        JSON_VALUE(l_string, '$.NAZIV')
    INTO
        l_zanr.ID,
        l_zanr.NAZIV
    FROM 
       dual; 

    if (nvl(l_zanr.NAZIV, ' ') = ' ') then   
       l_obj.put('h_message', 'Molimo unesite ispravan naziv zanra'); 
       l_obj.put('h_errcode', 101);
       raise e_iznimka;
    end if;


    out_json := l_obj;
    return false;

  EXCEPTION
     WHEN E_IZNIMKA THEN
            return true;
     WHEN OTHERS THEN
        --COMMON.p_errlog('p_check_klijenti',dbms_utility.format_error_backtrace,SQLCODE,SQLERRM, l_string);
        l_obj.put('h_message', 'Dogodila se greška u obradi podataka!'); 
        l_obj.put('h_errcode', 109);
        out_json := l_obj;
        return true;
  END f_check_zanr;


--check_glumac
--------------------------------------------
  function f_check_glumac(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) return boolean AS
      l_obj JSON_OBJECT_T;
      l_glumac glumac%rowtype;
      l_count number;
      l_id number;
      l_string varchar2(1000);
      l_search varchar2(100);
      l_page number; 
      l_perpage number;
  BEGIN
     l_obj := JSON_OBJECT_T(in_json);  
     l_string := in_json.TO_STRING;
     SELECT
        JSON_VALUE(l_string, '$.ID' ),
        JSON_VALUE(l_string, '$.IME'),
        JSON_VALUE(l_string, '$.PREZIME' ),
        JSON_VALUE(l_string, '$.DRZAVLJANSTVO' )
    INTO
        l_glumac.ID,
        l_glumac.IME,
        l_glumac.PREZIME,
        l_glumac.DRZAVLJANSTVO
    FROM 
       dual; 

    if (nvl(l_glumac.IME, ' ') = ' ') then   
       l_obj.put('h_message', 'Molimo unesite ime glumca'); 
       l_obj.put('h_errcode', 101);
       raise e_iznimka;
    end if;

    if (nvl(l_glumac.PREZIME, ' ') = ' ') then   
       l_obj.put('h_message', 'Molimo unesite prezime glumca'); 
       l_obj.put('h_errcode', 102);
       raise e_iznimka;
    end if;

    if (nvl(l_glumac.DRZAVLJANSTVO, ' ') = ' ') then   
       l_obj.put('h_message', 'Molimo unesite drzavljanstvo glumca'); 
       l_obj.put('h_errcode', 103);
       raise e_iznimka;
    end if;

    return false;


  EXCEPTION
     WHEN E_IZNIMKA THEN
            return true;
     WHEN OTHERS THEN
        --COMMON.p_errlog('p_check_klijenti',dbms_utility.format_error_backtrace,SQLCODE,SQLERRM, l_string);
        l_obj.put('h_message', 'Dogodila se greška u obradi podataka!'); 
        l_obj.put('h_errcode', 109);
        out_json := l_obj;
        return true;
  END f_check_glumac;



--check_reziser
-------------------------------------------
function f_check_reziser(in_json in JSON_OBJECT_T, out_json out JSON_OBJECT_T) return boolean AS
      l_obj JSON_OBJECT_T;
      l_reziser reziser%rowtype;
      l_count number;
      l_id number;
      l_string varchar2(1000);
      l_search varchar2(100);
      l_page number; 
      l_perpage number;
  BEGIN
     l_obj := JSON_OBJECT_T(in_json);  
     l_string := in_json.TO_STRING;
    SELECT
        JSON_VALUE(l_string, '$.ID' ),
        JSON_VALUE(l_string, '$.IME'),
        JSON_VALUE(l_string, '$.PREZIME' ),
        JSON_VALUE(l_string, '$.DRZAVLJANSTVO' )
    INTO
        l_reziser.id,
        l_reziser.IME,
        l_reziser.PREZIME,
        l_reziser.DRZAVLJANSTVO
    FROM 
       dual; 

    if (nvl(l_reziser.IME, ' ') = ' ') then   
       l_obj.put('h_message', 'Molimo unesite ime rezisera'); 
       l_obj.put('h_errcode', 101);
       raise e_iznimka;
    end if;

    if (nvl(l_reziser.PREZIME, ' ') = ' ') then   
       l_obj.put('h_message', 'Molimo unesite prezime rezisera'); 
       l_obj.put('h_errcode', 102);
       raise e_iznimka;
    end if;

    if (nvl(l_reziser.DRZAVLJANSTVO, ' ') = ' ') then   
       l_obj.put('h_message', 'Molimo unesite drzavljanstvo rezisera'); 
       l_obj.put('h_errcode', 103);
       raise e_iznimka;
    end if;

    out_json := l_obj;
    return false;

  EXCEPTION
     WHEN E_IZNIMKA THEN
            return true;
     WHEN OTHERS THEN
        --COMMON.p_errlog('p_check_klijenti',dbms_utility.format_error_backtrace,SQLCODE,SQLERRM, l_string);
        l_obj.put('h_message', 'Dogodila se greška u obradi podataka!'); 
        l_obj.put('h_errcode', 109);
        out_json := l_obj;
        return true;
  END f_check_reziser;


END FILTER;
